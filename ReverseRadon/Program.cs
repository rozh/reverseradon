﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Targets.Wrappers;
using ReverseRadon.Interfaces;
using ReverseRadon.Transform;
using ReverseRadon.Utils;

namespace ReverseRadon
{
    class Program
    {
        static void Main(string[] args)
        {
            var img = new ImageHelper();
            var filePath = "TestData/sinoFig4.png";
            var resultPath = "result.bmp";
            ITransform transform = null;

            var logger = LogManager.GetCurrentClassLogger();

            if (args.Length > 1)
            {
                if (args.Length > 2) resultPath = args[2];
                filePath = args[1];
                switch (args[0])
                {
                    case "r":
                        transform = new ReverseRadonTransform();
                        break;
                    case "f": 
                        transform = new RadonTransform();
                        break;
                    default:
                        logger.Fatal("Неправильный агрумент выбора алгоритма. Должен быть r или f");
                        break;
                }
            }
            else
            {
                Console.WriteLine("Использование:");
                Console.WriteLine($"{System.AppDomain.CurrentDomain.FriendlyName} [f/r] input.png result.bmp");
                Console.WriteLine($"[f/r] - f для прямого преобразования Радона, r для обратного преобразования Радона");
                Console.WriteLine($"input.png - входной файл");
                Console.WriteLine($"result.bmp - файл результата");
                Console.WriteLine($"");
                Console.WriteLine($"Пример для обратного преобразования:");
                Console.WriteLine($"{System.AppDomain.CurrentDomain.FriendlyName} r input.png result.bmp");
            }

            if (transform != null)
            {
                if (img.LoadImage(filePath))
                {
                    var matrix = transform.ApplyTranslation(img.GetGrayScaleMatrix());
                    img.SaveImage(matrix, resultPath);
                }
                else
                {
                    logger.Fatal($"Не удалось загрузить файл {filePath}");
                }
            }
        }
    }
}
