namespace ReverseRadon.Interfaces
{
    internal interface ITransform
    {
        double[,] ApplyTranslation(double[,] grayScaleMatrix);
    }
}