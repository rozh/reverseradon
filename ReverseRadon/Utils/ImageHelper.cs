﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using NLog;

namespace ReverseRadon.Utils
{
    class ImageHelper
    {
        private Logger _logger;

        public ImageHelper()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public bool LoadImage(string fileName)
        {
            _logger.Info("Загрузка изображения");
            try
            {
                Image = Image.FromFile(fileName);
            }
            catch (Exception e)
            {
                _logger.Error(e, "Ошибка загрузки изображения");
                return false;
            }

            return true;
        }

        public Image Image { get; protected set; }

        public double[,] GetGrayScaleMatrix()
        {
            _logger.Trace("Получение матрицы уровней свечения");

            var greyImage = new double[Image.Width, Image.Height];
            var bitmap = new Bitmap(Image);
            BitmapData bitmapData =
               bitmap.LockBits(new Rectangle(0, 0, Image.Width, Image.Height),
               ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* imagePointer = (byte*)bitmapData.Scan0;

                for (int i = 0; i < bitmapData.Height; i++)
                {
                    for (int j = 0; j < bitmapData.Width; j++)
                    {
                        greyImage[j, i] = ((imagePointer[0] +
                           imagePointer[1] + imagePointer[2]) / 3.0);
                        imagePointer += 4;
                    }
                    imagePointer += bitmapData.Stride - (bitmapData.Width * 4);
                }
            }
            bitmap.UnlockBits(bitmapData);
            bitmap.Dispose();

            return greyImage;
        }

        public bool SaveImage(double[,] matrix, string fileName)
        {
            _logger.Trace("Сохраннение изобржения из матрицы");

            var heihgt = matrix.GetLength(1);
            var width = matrix.GetLength(0);

            NormalizeMatrix(matrix);

            var bitmap = new Bitmap(width, heihgt);
            BitmapData bitmapData =
               bitmap.LockBits(new Rectangle(0, 0, width, heihgt),
               ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* imagePointer = (byte*)bitmapData.Scan0;

                for (int i = 0; i < bitmapData.Height; i++)
                {
                    for (int j = 0; j < bitmapData.Width; j++)
                    {
                        var color = (byte) (matrix[j, i] > 255 ? 255 : matrix[j, i]);
                        imagePointer[0] = color;
                        imagePointer[1] = color;
                        imagePointer[2] = color;
                        imagePointer += 4;
                    }
                    imagePointer += bitmapData.Stride - (bitmapData.Width * 4);
                }
            }
            bitmap.UnlockBits(bitmapData);

            bitmap.Save(fileName, ImageFormat.Bmp);
            return true;
        }

        void NormalizeMatrix(double[,] matrix)
        {
            var heihgt = matrix.GetLength(1);
            var width = matrix.GetLength(0);

            double max = 0;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < heihgt; j++)
                {
                    if (matrix[i, j] > max) max = matrix[i, j];
                    if (matrix[i, j] < 0 ) matrix[i, j] = 0;
                }
            }

            double normalizeFactor = 255 / max;


            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < heihgt; j++)
                {
                    matrix[i, j] = matrix[i, j] * normalizeFactor;
                }
            }
        }
    }
}
