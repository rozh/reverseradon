﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ReverseRadon.Utils
{
    static class Converters
    {
        public static Complex ToComplex(this double number)
        {
            return new Complex(number, 0);
        }

        public static Complex[] ToComplex(this double[] numbers)
        {
            var complex = new Complex[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                complex[i] = numbers[i].ToComplex();
            }
            return complex;
        }

        public static double ToDouble(this Complex number)
        {
            return number.Real;
        }

        public static double[] ToDouble(this Complex[] numbers)
        {
            var doubles = new double[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                doubles[i] = numbers[i].ToDouble();
            }
            return doubles;
        }
    }
}
