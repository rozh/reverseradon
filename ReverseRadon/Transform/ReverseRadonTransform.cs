﻿using System;
using System.Diagnostics;
using MathNet.Numerics.IntegralTransforms;
using NLog;
using ReverseRadon.Interfaces;
using ReverseRadon.Utils;

namespace ReverseRadon.Transform
{
    internal class ReverseRadonTransform : ITransform
    {
        private double _deltaE;
        private double _deltaO;
        private readonly Logger _logger;
        private int _Ne;
        private int _No;
        private int _R;

        public ReverseRadonTransform()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public double[,] ApplyTranslation(double[,] grayScaleMatrix)
        {
            _logger.Trace("Применение обратного преобразование Радона");

            var sw = new Stopwatch(); //измеряет время работы программы
            sw.Start();

            _No = grayScaleMatrix.GetLength(0); //количество проекций [0;Pi], матрица яркости пикселей, ширина матрицы
            _Ne = grayScaleMatrix.GetLength(1); //диапазон [-R;R], высота матрицы
            _R = _Ne / 2; //радиус
            _deltaE = 2 * _R / (double) _Ne; //шаг r 
            _deltaO = Math.PI / _No; //шаг fi

            var matrix = FilterMatrix(grayScaleMatrix);
            var result = ReverseProjection(matrix);

            sw.Stop();
            _logger.Trace($"Обратное преобразование Радона выполнено за {sw.ElapsedMilliseconds} мс");

            return result;
        }

        private double[,] ReverseProjection(double[,] matrix)
        {
            var nx = _No; //количество проекций для угла Фи
            var ny = _Ne; //поворот проекции

            var f = new double[nx, ny];
            var x0 = _R;
            var y0 = _R;

            for (var i = 0; i < nx; i++)
            for (var j = 0; j < ny; j++)
            {
                var x = -x0 + i;
                var y = -y0 + j;

                for (var k = 0; k < _No; k++)
                {
                    var o = k * _deltaO;
                    var r = x * Math.Cos(o) + y * Math.Sin(o);
                    var l = (int) ((r + _R) / _deltaE);
                    var s = r + _R - l * _deltaE;

                    if (l + 1 < _Ne && l >= 0)
                        f[nx - i - 1, ny - j - 1] += Math.PI / _No *
                                                     (matrix[k, l] * (_deltaE - s) + matrix[k, l + 1] * s) / _deltaE;
                }
            }

            return f;
        }

        private double[,] FilterMatrix(double[,] grayScaleMatrix)
        {
            _logger.Trace("Применение фильтра");

            var sw = new Stopwatch();
            sw.Start();

            var result = ApplyFilter(grayScaleMatrix);

            sw.Stop();
            _logger.Trace($"Применение фильтра выполнено за {sw.ElapsedMilliseconds} мс");

            return result;
        }

        private double[,] ApplyFilter(double[,] grayScaleMatrix)
        {
            var t = new double[_Ne];
            var p = new double[_No, _Ne];

            for (var k = 0; k < _No; k++)
            {
                for (var l = 0; l < _Ne; l++)
                    t[l] = grayScaleMatrix[k, l];

                var complex = t.ToComplex();
                Fourier.Forward(complex);

                var norm = (_Ne * _deltaE);
                for (int l = 0; l < _Ne / 2; l++)
                {
                    complex[l] *= l / norm;
                    complex[_Ne - l - 1] *= l / norm;
                }

                Fourier.Inverse(complex);
                t = complex.ToDouble();

                for (var l = 0; l < _Ne; l++)
                    p[k, l] = t[l];
            }

            return p;
        }
    }
}