﻿using System;
using System.Diagnostics;
using NLog;
using ReverseRadon.Interfaces;

namespace ReverseRadon.Transform
{
    internal class RadonTransform : ITransform
    {
        private readonly Logger _logger;
        private int _r;
        private double _deltaPhi;
        private double _deltaRo;
        private int _l, _k;
        private int _m, _n;
        private double _phi, _ro;
        private double _phiMin, _phiMax;
        private double _roMin;

        public RadonTransform()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public double[,] ApplyTranslation(double[,] grayScaleMatrix)
        {
            _logger.Trace("Применение преобразование Радона");

            var sw = new Stopwatch(); //измеряет время работы программы
            sw.Start();

            _m = grayScaleMatrix.GetLength(0); //ширина матрицы по Ох
            _n = grayScaleMatrix.GetLength(1); //высота матрицы по Оу
            _phiMin = 0; //phi, количество проекций [0;Pi], ширина матрицы
            _phiMax = Math.PI;
            //ro_min = -m/2;//ro, диапазон [-R;R], высота матрицы
            //ro_max = m/2;
            _roMin = -Math.Sqrt(_m * _m + _n * _n) / 2; //ro, диапазон [-R;R], высота матрицы
            _r = grayScaleMatrix.GetLength(1) / 2; //радиус           
            _l = _n; //количество углов phi
            _deltaPhi = (_phiMax - _phiMin) / _l; //шаг fi
            _k = _m; //количество проекций ro
            _deltaRo = Math.Sqrt(_m * _m + _n * _n) / _k; //шаг r 
            var result = ReverseProjection(grayScaleMatrix);

            sw.Stop();
            _logger.Trace($"Преобразование Радона выполнено за {sw.ElapsedMilliseconds} мс");

            return result;
        }

        private double[,] ReverseProjection(double[,] matrix)
        {
            var f = new double[_k, _l];
            var x0 = _r;
            var y0 = _r;

            for (var i = 0; i < _k; i++)
            for (var j = 0; j < _l; j++)
                f[i, j] = 0;

            for (var x = -_r; x < _r; x++)
            {
                var xx = x0 + x;
                for (var y = -_r; y < _r; y++)
                {
                    var yy = y0 + y;
                    for (var j = 0; j < _l; j++) //цикл по phi
                    {
                        _phi = _phiMin + j * _deltaPhi;
                        _ro = x * Math.Cos(_phi) + y * Math.Sin(_phi);
                        var i = (int) ((_ro - _roMin) / _deltaRo);
                        if (i >= 0 && i < _k)
                            f[j, i] += matrix[xx, yy];
                    }
                }
            }
            return f;
        }
    }
}